<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XmlFileStatus extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'status',
        'class'
    ];


    public function xml_files(){
        $this->hasMany('App\XmlFile');
    }
}
