<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NodeStatus extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'status',
        'class'
    ];

    public function nodes(){
        $this->hasMany('App\Node');
    }
}
