<?php

namespace App\Http\Controllers;

use App\Person;
use App\XmlFile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ServerSideEventController extends Controller
{
    public function index()
    {
        $result['people_count'] = Person::count();
        $result['xml_files'] = XmlFile::all()->toArray();

        return $result;
    }
}
