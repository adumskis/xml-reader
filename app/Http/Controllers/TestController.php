<?php

namespace App\Http\Controllers;

use App\Node;
use App\XmlFile;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Facades\Response;
use Faker;

class TestController extends Controller
{
   public function testParse(){
       $array = [];
       $faker = Faker\Factory::create();


       for($i = 0; $i <= 10000; $i++){
            $array['persons'][] = [
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'address' => $faker->address,
                'city' => $faker->city,
                'postcode' => $faker->postcode,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'age' => rand(18, 60),
                'moto' => $faker->sentence
            ];
       }

       $formatter = Formatter::make($array, Formatter::XML);


       return Response::make($formatter->toXml(), '200')->header('Content-Type', 'text/xml');



       $nodes = Node::all();

       foreach($nodes as $node){
           echo '<pre>';
           var_dump($node->node);
           echo '</pre>';
           echo '<hr>';
       }

       dd();

       $dom_doc = new \DOMDocument();

       $reader = new \XMLReader();
       $reader->open('xml/products_short.xml');

       $xml_file = XmlFile::first();

       while ($reader->read() && $reader->name !== 'product');

       while($reader->name == 'product'){
           $product = simplexml_import_dom($dom_doc->importNode($reader->expand(), true));
           Node::create([
               'xml_file_id' => $xml_file->id,
               'node' => serialize($this->parseNode($product))
           ]);
           $reader->next('product');
       }
   }

    private function parseNode($node){
        $result = [];
        foreach($node->children() as $key =>$value){
            if($value->children()->count()){
                foreach($value->children() as $child_key => $child_value){
                    $result[$key][][$child_key] = $this->parseNode($child_value);
                }
            } else {
                $result[$key] = $value->__toString();
            }
            if($value->attributes()){
                foreach($value->attributes() as $attr_key => $attr_value){
                    $result[$attr_key] = $attr_value->__toString();
                }
            }
        }
        return $result;
    }
}
