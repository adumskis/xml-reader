<?php

namespace App\Http\Controllers;

use App\Jobs\ParseXmlFile;
use App\XmlFile;
use App\XmlFileStatus;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\XmlFileRequest;

class XmlFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $xml_files = XmlFile::all();

        return view('xml_file.index', compact('xml_files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('xml_file.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(XmlFileRequest $request)
    {
        $file_name = time().'_'.str_random(10).'.xml';
        $file = $request->file('xml_file');
        $file->move(public_path('xml'), $file_name);
        $xml_file = XmlFile::create([
            'original_name' => $file->getClientOriginalName(),
            'filename' => $file_name,
            'path' => public_path('xml/'.$file_name)
        ]);

        $this->dispatch(new ParseXmlFile($xml_file));

        return redirect()->route('xml-file.show', $xml_file->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $xml_file = XmlFile::findOrFail($id);

        return view('xml_file.show', compact('xml_file'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
