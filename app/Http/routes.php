<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test/test-parse', [
    'as' => 'test_parse', 'uses' => 'TestController@testParse'
]);

Route::resource('xml-file', 'XmlFileController');
Route::resource('product', 'ProductController');
Route::resource('person', 'PersonController');
Route::get('sse', 'ServerSideEventController@index');