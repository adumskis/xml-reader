<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\NodeStatus;
use App\XmlFile;
use App\XmlFileStatus;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Node;
use Illuminate\Foundation\Bus\DispatchesJobs;

class ParseXmlFile extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    protected $xml_file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(XmlFile $xml_file)
    {
        $this->xml_file = $xml_file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dom_doc = new \DOMDocument();
        $reader = new \XMLReader();
        $reader->open($this->xml_file->path);

        $xml_file = $this->xml_file;


        while ($reader->read() && $reader->name !== 'person');

        while($reader->name == 'person'){
            $product = simplexml_import_dom($dom_doc->importNode($reader->expand(), true));
            $node = Node::create([
                'xml_file_id' => $xml_file->id,
                'node' => $this->parseNode($product)
            ]);

            $this->dispatch(new ParseNode($node));

            $reader->next('person');
        }
    }

    private function parseNode($node){
        $result = [];
        foreach($node->children() as $key =>$value){
            if($value->children()->count()){
                foreach($value->children() as $child_key => $child_value){
                    $result[$key][][$child_key] = $this->parseNode($child_value);
                }
            } else {
                $result[$key] = $value->__toString();
            }
            if($value->attributes()){
                foreach($value->attributes() as $attr_key => $attr_value){
                    $result[$attr_key] = $attr_value->__toString();
                }
            }
        }
        return $result;
    }
}
