<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Person;
use App\Product;
use App\ProductAttribute;
use App\Node;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class ParseNode extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $node;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Node $node)
    {
        $this->node = $node;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $timeStart = microtime(true);

        $node_data = $this->node->node;

        $person = Person::create($node_data);

        $diff = microtime(true) - $timeStart;
        $sec = intval($diff);
        $micro = $diff - $sec;
        $this->node->update([
            'time' => round($micro * 1000, 4)
        ]);
    }
}
