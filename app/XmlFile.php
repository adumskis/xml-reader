<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\XmlFileStatus;

class XmlFile extends Model
{
    protected $fillable = [
        'original_name',
        'filename',
        'path',
        'status_id'
    ];

    public function nodes(){
        return $this->hasMany('App\Node');
    }

    public function status(){
        return $this->belongsTo('App\XmlFileStatus');
    }

    public function setStatus($status){
        $this->status_id = XmlFileStatus::where('status', $status)->firstOrFail()->id;
        $this->save();
    }
}
