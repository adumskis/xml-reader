<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'reference',
        'name',
        'manufacturer',
        'price',
        'wholesale_price',
        'quantity',
        'category',
        'description'
    ];

    public function product_attributes(){
        return $this->hasMany('App\ProductAttribute');
    }
}
