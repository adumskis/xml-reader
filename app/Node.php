<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\NodeStatus;

class Node extends Model
{
    protected $fillable = [
        'xml_file_id',
        'node',
        'status_id',
        'time'
    ];

    public $timestamps = false;

    public function xml_file(){
        return $this->belongsTo('App\XmlFile');
    }

    public function getNodeAttribute($node){
        return unserialize($node);
    }

    public function setNodeAttribute($node){
        $this->attributes['node'] = serialize($node);
    }

    public function status(){
        return $this->belongsTo('App\NodeStatus');
    }

    public function setStatus($status){
        $this->status_id = NodeStatus::where('status', $status)->firstOrFail()->id;
        $this->save();
    }
}
