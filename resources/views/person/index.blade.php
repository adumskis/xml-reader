@extends('layout')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                People list
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            First name
                        </th>
                        <th>
                            Last name
                        </th>
                        <th>
                            Phone
                        </th>
                        <th>
                            Company
                        </th>
                        <th>
                            Department
                        </th>
                        <th>
                            Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($people as $person)
                        <tr>
                            <td>
                                {{ $person->id }}
                            </td>
                            <td>
                                {{ $person->first_name }}
                            </td>
                            <td>
                                {{ $person->last_name }}
                            </td>
                            <td>
                                {{ $person->phone }}
                            </td>
                            <td>
                                {{ $person->company }}
                            </td>
                            <td>
                                {{ $person->department }}
                            </td>
                            <td>
                                <a class="btn btn-default" href="{{ route('person.show', $person->id) }}">Show</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $people->render() !!}
            </div>
            <div class="panel-footer">
                Total: {{ $people->total() }}
            </div>
        </div>
    </div>
@stop