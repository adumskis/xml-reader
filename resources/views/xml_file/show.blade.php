@extends('layout')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                XML file: #{{ $xml_file->id }}
            </div>
            <div class="panel-body">
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt>ID:</dt>
                        <dd>{{ $xml_file->id }}</dd>

                        <dt>Original name:</dt>
                        <dd>{{ $xml_file->original_name }}</dd>

                        <dt>File name:</dt>
                        <dd>{{ $xml_file->filename }}</dd>

                        <dt>Path:</dt>
                        <dd>{{ $xml_file->path }}</dd>

                        <dt>Created at:</dt>
                        <dd>{{ $xml_file->created_at }}</dd>

                        <dt>Updated at:</dt>
                        <dd>{{ $xml_file->updated_at }}</dd>
                    </dl>
                </div>
            </div>
            <div class="panel-footer">
            </div>
        </div>
    </div>
@stop