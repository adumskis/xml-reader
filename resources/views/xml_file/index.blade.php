@extends('layout')

@section('content')
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            XML files list
            <a class="btn btn-primary" href="{{ route('xml-file.create') }}">Create</a>
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Original name
                        </th>
                        <th>
                            Filename
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                            Created at
                        </th>
                        <th>
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($xml_files as $xml_file)
                        <tr>
                            <td>
                                {{ $xml_file->id }}
                            </td>
                            <td>
                                {{ $xml_file->original_name }}
                            </td>
                            <td>
                                {{ $xml_file->filename }}
                            </td>
                            <td>
                                {{ $xml_file->status }}
                            </td>
                            <td>
                                {{ $xml_file->created_at }}
                            </td>
                            <td>
                                <a class="btn btn-default" href="{{ route('xml-file.show', $xml_file->id) }}">Show</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            Total: {{ count($xml_files) }}
        </div>
    </div>
</div>
@stop