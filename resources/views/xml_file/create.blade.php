@extends('layout')

@section('content')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Create new XML file
                <a class="btn btn-primary" href="{{ route('xml-file.create') }}">Create</a>
            </div>
            <div class="panel-body">
                @include('_errors')

                {!! Form::open([
                    'method' => 'POST',
                    'route' => 'xml-file.store',
                    'files' => true
                ]) !!}
                    <div class="form-group">
                        {!! Form::label('xml_file', 'XML file') !!}
                        {!! Form::input('file', 'xml_file', null) !!}
                    </div>

                    {!! Form::submit('submit', ['class' => 'btn btn-default']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop