@extends('layout')

@section('content')
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            Products list
        </div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Name
                        </th>
                        <th>
                            Reference
                        </th>
                        <th>
                            Wholesale price
                        </th>
                        <th>
                            Price
                        </th>
                        <th>
                            Quantity
                        </th>
                        <th>
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>
                                {{ $product->id }}
                            </td>
                            <td>
                                {{ $product->name }}
                            </td>
                            <td>
                                {{ $product->reference }}
                            </td>
                            <td>
                                {{ $product->wholesale_price }}
                            </td>
                            <td>
                                {{ $product->price }}
                            </td>
                            <td>
                                {{ $product->quantity }}
                            </td>
                            <td>
                                <a class="btn btn-default" href="{{ route('product.show', $product->id) }}">Show</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            Total: {{ count($product) }}
        </div>
    </div>
</div>
@stop