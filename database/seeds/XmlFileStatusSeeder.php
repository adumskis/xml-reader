<?php

use Illuminate\Database\Seeder;
use App\XmlFileStatus;

class XmlFileStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Starting seeding XML file status table');

        XmlFileStatus::create([
            'name' => 'Waiting in queue',
            'status' => 'in_queue',
            'class' => 'primary'
        ]);

        XmlFileStatus::create([
            'name' => 'Parsing',
            'status' => 'parsing',
            'class' => 'warning'
        ]);

        XmlFileStatus::create([
            'name' => 'Parsed',
            'status' => 'parsed',
            'class' => 'success'
        ]);

        XmlFileStatus::create([
            'name' => 'Error while parsing',
            'status' => 'error',
            'class' => 'error'
        ]);

        $this->command->info('Finished seeding XML file status table');
    }
}
