<?php

use Illuminate\Database\Seeder;
use App\NodeStatus;

class NodeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Starting seeding node status table');

        NodeStatus::create([
            'name' => 'Waiting in queue',
            'status' => 'in_queue',
            'class' => 'primary'
        ]);

        NodeStatus::create([
            'name' => 'Parsing',
            'status' => 'parsing',
            'class' => 'warning'
        ]);

        NodeStatus::create([
            'name' => 'Parsed',
            'status' => 'parsed',
            'class' => 'success'
        ]);

        NodeStatus::create([
            'name' => 'Error while parsing',
            'status' => 'error',
            'class' => 'error'
        ]);

        $this->command->info('Finished seeding node status table');
    }
}
