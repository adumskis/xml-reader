<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXmlFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xml_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('original_name');
            $table->string('filename');
            $table->string('path');
            $table->timestamps();
            $table->integer('status_id')->unsigned()->nullable();

            $table->foreign('status_id')->references('id')->on('xml_file_statuses')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('xml_files');
    }
}
