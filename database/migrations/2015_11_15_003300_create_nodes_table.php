<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('xml_file_id')->unsigned();
            $table->text('node');
            $table->integer('status_id')->unsigned()->nullable();
            $table->integer('time');

            $table->foreign('status_id')->references('id')->on('node_statuses')->onDelete('set null');
            $table->foreign('xml_file_id')->references('id')->on('xml_files')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nodes');
    }
}
